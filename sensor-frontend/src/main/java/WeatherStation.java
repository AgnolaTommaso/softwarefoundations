import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;

import java.util.concurrent.ThreadLocalRandom;

public class WeatherStation {
    final String SERVER_URL= "http://localhost:8080/weatherstation";

    public static void main (String[] args) {
        System.out.println("Frontend up");
        WeatherStation milano = new WeatherStation("Milano", 3);
        WeatherStation roma = new WeatherStation("Roma", 3);
        WeatherStation manno = new WeatherStation("Manno", 3);

    }

    private String location;
    private int requestPerSecond;
    public WeatherStation(String location, int requestPerSecond){
        this.location = location;
        this.requestPerSecond = requestPerSecond;
    }
    public void run(){
        double currentTemperature = ThreadLocalRandom.current().nextDouble(-10, 40);
        HttpResponse<JsonNode> response = Unirest.post(SERVER_URL)
                .header("accept", "application/json")
                .field("location", this.location)
                .field("temperature", String.valueOf(currentTemperature))
                .asJson();
        System.out.println(response);
    }
}
