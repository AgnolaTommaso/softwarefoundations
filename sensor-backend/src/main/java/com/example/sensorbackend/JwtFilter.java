package com.example.sensorbackend;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureException;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Arrays;

public class JwtFilter extends GenericFilterBean {
    String[] filterMethods = new String[] {"POST", "PUT", "DELETE"};
    public void doFilter(final ServletRequest req, final ServletResponse res, final FilterChain chain) throws IOException, ServletException {
        final HttpServletRequest request = (HttpServletRequest) req;
        if(Arrays.asList(filterMethods).contains(request.getMethod())) {
            final String authHeader = request.getHeader("authorization");

            if (authHeader == null || !authHeader.startsWith("Bearer ")) {
                throw new ServletException("Missing or invalid Authorization header");
            }

            final String token = authHeader.substring(7);
            try {
                final Claims claims = Jwts.parser().setSigningKey("secret").parseClaimsJws(token).getBody();
                request.setAttribute("claims", claims);
                // use the claims somewhere
            } catch (final SignatureException e) {
                // throws exception when expired and the signature don't match
                throw new ServletException("Invalid token");
            }
        }
        chain.doFilter(req, res);
    }
}