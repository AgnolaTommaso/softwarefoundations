package com.example.sensorbackend.Client;

import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import org.apache.http.entity.ContentType;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

public class Client {

    private static final String SERVER_URL = "http://35.233.240.128/back/";
    private static final String USERNAME = "user";
    private static final String PASS = "user";

    private static final Random rand = new Random();

    public static void main(String args[]) throws InterruptedException, KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
        Double value = 25d;
        int index = 0;

        String token = Unirest.post(SERVER_URL+"token?user={username}&password={password}")
                .routeParam("username", USERNAME)
                .routeParam("password", PASS)
                .asJson()
                .getBody()
                .getObject()
                .get("content")
                .toString();
        System.out.println(token);
        while(true) {
            int prod = (rand.nextInt() < 0 ) ? -1 : 1;
            if(index % 10 == 0) value = value + (5 * prod) ;
            value = (rand.nextDouble() * prod) + value;

            Unirest.post(SERVER_URL+"sensors")
                    .header("Content-Type", ContentType.APPLICATION_JSON.getMimeType())
                    .header("Authorization", "Bearer "+token)
                    .body("{\"name\":\"sensor1\", \"type\":\"temp\", \"value\":"+value+"}")
                    .asJson();
            index++;
            Thread.sleep(10000);
        }
    }
}
