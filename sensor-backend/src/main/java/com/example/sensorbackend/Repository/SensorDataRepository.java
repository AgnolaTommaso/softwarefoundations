package com.example.sensorbackend.Repository;

import com.example.sensorbackend.Model.SensorData;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "sensors", path = "sensors")
public interface SensorDataRepository extends PagingAndSortingRepository<SensorData, Long> {
    List<SensorData> findByName(@Param("name") String name);
    List<SensorData> findByType(@Param("type") String type);
}
