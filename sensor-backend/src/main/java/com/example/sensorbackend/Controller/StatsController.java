package com.example.sensorbackend.Controller;

import com.example.sensorbackend.Model.SensorData;
import com.example.sensorbackend.Repository.SensorDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryLinksResource;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.RepresentationModelProcessor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RepositoryRestController
@RequestMapping("/stats")
public class StatsController implements RepresentationModelProcessor<RepositoryLinksResource> {

    @Autowired
    private SensorDataRepository repository;

    private String instanceName = "";

    public StatsController() {
        String timestamp = (new Date().getTime()+"");
        instanceName = timestamp.substring(timestamp.length() - 4);
        System.out.println("*** "+instanceName);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/ping")
    public @ResponseBody
    ResponseEntity<?> ping() {
        EntityModel resource = new EntityModel(new HashMap<String,String>(){{put("pong",instanceName);}});
        return ResponseEntity.ok(resource);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/mean")
    public @ResponseBody
    ResponseEntity<?> getMeanByType(@RequestParam(value = "type") String type) {
        List<SensorData> sensorDatas = repository.findByType(type);

        double mean = sensorDatas.stream().mapToDouble(sd -> sd.getValue()).reduce((x, y) -> x + y).getAsDouble() / sensorDatas.size();

        EntityModel resource = new EntityModel(new HashMap<String,Double>(){{put("mean",mean);}});
        resource.add(linkTo(methodOn(StatsController.class).getMeanByType(type)).withSelfRel());

        return ResponseEntity.ok(resource);
    }

    @Override
    public RepositoryLinksResource process(RepositoryLinksResource model) {
        model.add(linkTo(methodOn(StatsController.class).getMeanByType(null)).withRel("mean"));
        return model;
    }
}