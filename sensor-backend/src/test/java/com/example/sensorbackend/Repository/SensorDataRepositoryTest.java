package com.example.sensorbackend.Repository;

import com.example.sensorbackend.Model.SensorData;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;


@RunWith(SpringRunner.class)
@DataJpaTest
public class SensorDataRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private SensorDataRepository sensorDataRepository;

    @Test
    public void findByNameTest() {
        SensorData sensor1 = new SensorData();
        sensor1.setName("Sensor_1");

        entityManager.persist(sensor1);
        entityManager.flush();

        List<SensorData> found = sensorDataRepository.findByName(sensor1.getName());

        assertEquals(1, found.size());
        assertEquals(found.get(0), sensor1);
    }

    @Test
    public void findByTypeTest() {
        SensorData sensor1 = new SensorData();
        sensor1.setName("Sensor_1");
        sensor1.setType("Type_1");

        entityManager.persist(sensor1);
        entityManager.flush();

        List<SensorData> found = sensorDataRepository.findByType(sensor1.getType());

        assertEquals(1, found.size());
        assertEquals(found.get(0), sensor1);
    }
}
